
package vcreature.mainSimulation;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.math.Vector3f;
import vcreature.phenotype.Creature;
import vcreature.phenotype.Block;
import com.jme3.scene.Node;

public class Sticks extends Creature
{
  final int segments = 2; // Number of segments to add.
  public Sticks(PhysicsSpace physicsSpace, Node rootNode)
  {
    super(physicsSpace, rootNode);

    // Body block @ (0.0, 1.0, 0.0) ext (4.7992725, 0.5, 0.62814397)
    Vector3f rootCenter = new Vector3f( 0.0f, 1.0f, 0.0f);
    Vector3f rootSize = new Vector3f( 4.7992725f, 0.5f, 0.62814397f);
    Block root = addRoot(rootCenter, rootSize);

    // Child block @ (-9.598545, 0.5, 0.62814397) ext (4.7992725, 0.5, 0.62814397) pj (-4.7992725, 0.0, 0.62814397) cj (4.7992725, 0.5, 0.0) axis (0.0, 1.0, 0.0)
    Vector3f s1Center  = new Vector3f(-9.598545f, 0.5f, 0.62814397f);
    Vector3f s1Size = new Vector3f(4.7992725f, 0.5f, 0.62814397f);
    Vector3f pivotA = new Vector3f(-4.7992725f, 0.0f, 0.62814397f); // Center of hinge in the block's coordinates
    Vector3f pivotB = new Vector3f(4.7992725f, 0.5f, 0.0f); // Center of hinge in the block's coordinates
    float[] eulerAngles = {0,0,0};
    Block seg1 = addBlock(eulerAngles, s1Size, root, pivotA, pivotB, new Vector3f(0.0f, 1.0f, 0.0f));
    
    root.setMaterial(Block.MATERIAL_GREEN);
    seg1.setMaterial(Block.MATERIAL_BLUE);   
   }
}